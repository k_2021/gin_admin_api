# 定义当前项目名称
pro_name="gin_admin_api"
# 给当前赋予执行权限
chmod +x ./$pro_name
# 重启，如果已经存在就关闭
if pgreg -x $pro_name > /dev/null
then
  echo "${pro_name} 已经运行"
  echo "关闭${pro_name}项目"
  if ps -a | grep $pro_name | awk "{print $1}"
    then
      echo "启动项目 ${pro_name}"
      ./$pro_name > /dev/null 2>&1 &
      echo "启动${pro_name}成功"
  fi
else
  echo "启动项目${pro_name}"
  ./$pro_name > /dev/null 2>&1 &
  echo "启动${pro_name}成功"
fi